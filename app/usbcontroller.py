# this import usb 
import usb.core 
import usb.util


class usbcontroller:
    def __init__(self):
        # inisiasi untuk akses ke usb
        # 28e0:0a05 Prasimax QR Generator
        self.dev=usb.core.find(idVendor=0x28e0, idProduct=0x0a05)
        if self.dev is None:
            raise ValueError('Device not found')
        else:
            print("QR display terdeteksi")

        self.dev.set_configuration()

    def writeToLCD(self, teks):
        self.dev.write(0x1, teks)

# this line for check the code
cont = usbcontroller()
cont.write("anjir")