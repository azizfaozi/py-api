# this import usb 
import usb.core 
import usb.util

# this library import mantap
from flask import render_template
from flask import abort
from flask import jsonify
from flask import request
from app import app

# this variable just for testing in this api
tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol', 
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web', 
        'done': False
    }
]

# index 
@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'prasimax'}
    return render_template('index.html', title='Home', user=user)

# just for get api
@app.route('/get/<int:task_id>', methods=['GET'])
def get_task(task_id):
    task=[task for task in tasks if task['id']==task_id]
    if len(task)==0:
        abort(404)
    return jsonify({'task':task[0]})

# post for api
@app.route('/post', methods=['POST'])
def create_task():
    if not request.json or not 'title' in request.json:
        abort(400)
    task = {
        'id': tasks[-1]['id'] + 1,
        'title': request.json['title'],
        'description': request.json.get('description', ""),
        'done': False
    }
    tasks.append(task)
    return jsonify({'task': task}), 201